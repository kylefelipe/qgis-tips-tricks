# -*- coding: utf-8 -*-
from math import sqrt, pi
@qgsfunction(args='auto', group='Lista')
def polygon_sphericity(feature, parent)-> float:
    """Calcula o grau de esfericidade do poligono.
    Quanto mais próximo de 1, mais circular é o vetor.
    <p><h4>Syntax</h4><font color="blue">polygon_sphericity</font>()</p>
    <p><h4>Example</h4>polygon_sphericity()-> 0.607763480072298 </p>"""
    geom = feature.geometry()
    constant = 1/(4*pi)
    return sqrt(geom.area()/(constant*(geom.length()**2)))
