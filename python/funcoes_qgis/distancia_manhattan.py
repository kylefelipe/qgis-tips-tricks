from qgis.core import *
from qgis.gui import *

@qgsfunction(args='auto', group='Custom')
def distancia_manhattan(point1, point2, feature, parent):
    """
    Calcula a distancia de manhattan entre dois pontos.
    <h2>Example usage:</h2>
    <ul>
    <li>distancia_manhattan(ponto1, ponto2) -> 13</li>
    </ul>
    """
    result1 = abs(point1.asPoint().x() - point2.asPoint().x())
    result2 = abs(point1.asPoint().y() - point2.asPoint().y())
    
    return result1 + result2