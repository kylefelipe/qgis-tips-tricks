#!/usr/bin/python3

"""Aplica o gdal_merge em arquivos iguais em uma mesma pasta
Padrão nome:
LANDSAT: Font > https://www.usgs.gov/media/files/landsat-collection-1-level-1-product-definition
LXSS_LLLL_PPPRRR_YYYYMMDD_yyyymmdd_CC_TX_FT.ext
L = Landsat
X = sensor: O > OLI, T = TIRS, C = Combined TIRS and OLI
SS = Landsat Satelite (08 for Landsat 8)
LLLL = Processing Level (L1TP, L1GT, L1GS)
PPP = Satellite orbit location in reference to the Worldwide Reference System-2 (WRS-2)
path of the product
RRR = Satellite orbit location in reference to the WRS-2 row of the product
YYYY = Acquisition year of the image
MM = Acquisition month of the image
DD = Acquisition day of the image
yyyy = Processing year of the image
mm = Processing month of the image
dd = Processing day of the image
CC = Collection number (e.g. 01)
TX = Tier of the image: "RT" for Real-time, "T1" for Tier 1 (highest quality), "T2" for Tier 2
_FT = File type, where FT equals one of the following: image band file number (B1–B11),
MTL (metadata file), BQA (Quality Band file), MD5 (checksum file), ANG (angle
coefficient file)
.ext = File extension, where .TIF equals GeoTIFF file extension, and .txt equals text
extension
"""

from os import path, walk

class FileName:

    def __init__(self, file_name):
        padrao_satelite = {
            'constelacao' : {
                "L": "Landsat",
                },
            'sensor': {'O': 'OLI',
                        'T': 'TIRS',
                        'C': 'TIRS + OLI'},
            'tier': {'RT': 'Real-time',
                     'T1': 'Tier 1',
                     'T2': 'Tier 2'},
            'file_type': {'B': 'Band',
                          'MTL': 'Metadata',
                          'BQA': 'Quality Band file',
                          'MD5': "Checksum",
                          'ANG': 'Angle Coefficient'
                          },
            }

        constelacao, processing_level, pathrow, \
        acquisition, processin_date, collection_number,\
        tier_image, file_type = file_name.upper().split('_')

        self.name = file_name
        self.constelacao = padrao_satelite['constelacao'][constelacao[0]]
        self.sensor = padrao_satelite['sensor'][constelacao[1]]
        self.satelite = constelacao[2:]
        self.processing_level = processing_level
        self.path = pathrow[:3]
        self.row = self.name[3:]
        self.acquisition = f"{acquisition[:4]}-{acquisition[4:6]}-{acquisition[6:]}"
        self.processing = f"{processin_date[:4]}-{processin_date[4:6]}-{processin_date[6:]}"
        self.collection = collection_number
        self.tier = tier_image
        self.file_type = None
        self.extension = file_type.split('.')[1]
        self.band = None
        if file_type[1] in [str(i) for i in range(10) ]:
            self.file_type = 'Band'
            self.band = file_type.split('.')[0][:1]
        else:
            self.file_type = padrao_satelite['file_type'][file_type.split('.')[0]]
        
    
    def __repr__(self):
        return self.name
    

file_name = "LC08_L1TP_219072_20190121_20190121_01_RT_B1.TIF"
arquivo = FileName(file_name)
root_img = '/media/kylefelipe/dados/imagens_landsat'
pastas = list(walk(root_img))[1:]

root, folders, arquivos = pastas[0]
