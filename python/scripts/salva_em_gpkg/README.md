# QGIS tips & tricks - PYTHON - salva_em_gpkg
__Python__: 3.6

This script saves all vectors layers form a QGIS project to a Geopackage file in the same folder.

USAGE:
Open QGIS Python Terminal (Ctrl+Alt+P)
Click "Show editor"
Paste the code there or open the script.
Click *Run Script*
