# Font:
# https://gis.stackexchange.com/questions/285346/add-layers-to-geopackage-using-pyqgis-qgis-3

from re import sub
from os import path
from qgis.core import (QgsProject,
                       QgsVectorFileWriter,
                       QgsMapLayerType
                       )

qgis = QgsApplication([], False)

layers = [layer for id_layer, layer in QgsProject.instance().mapLayers().items()]
project_path = QgsProject.instance().absolutePath()


def write_layer(layer):
    # Pode alterar o "project_layers" por outro nome se quiser.
    output_file = path.join(project_path, "project_layers.gpkg")
    layer_name = '_'.join(layer.name()).lower()
    options = None
    options = QgsVectorFileWriter.SaveVectorOptions()
    options.driverName = "GPKG"
    options.layerName = layer_name
    options.actionOnExistingFile = QgsVectorFileWriter.CreateOrOverwriteFile
        
    if path.isfile(output_file):
        options.actionOnExistingFile = QgsVectorFileWriter.CreateOrOverwriteLayer
    
    _writer = QgsVectorFileWriter.writeAsVectorFormat(layer, output_file, options)
    if _writer[0] == QgsVectorFileWriter.NoError:
        print(f"Camada {layer_name} salva com sucesso")
        print(f"em {output_file}")
    else:
        print(_writer[1])

for layer in layers:
    if layer.type() == QgsMapLayerType.VectorLayer:
        write_layer(layer)

print("End!")
