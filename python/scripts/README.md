# QGIS tips & tricks - PYTHON - SCRIPTS

Scripts feitos para rodar no terminal python do qgis  
Assista ao vídeo de como utilizar scripts python no QGIS:  
[![Assista ao vídeo dPython + Qgis = Mais tempo na Vida](https://img.youtube.com/vi/0kaYuPeM0wI/0.jpg)](https://www.youtube.com/watch?v=0kaYuPeM0wI)  

* [salva_camadas](https://github.com/kylefelipe/qgis-tips-tricks/tree/master/python/salva_camadas): Esse script salva todas as camadas do projeto em um arquivo kml dentro das pastas das respectivas camadas (não funciona em camadas de texto delimitado).

* [save_selected](https://github.com/kylefelipe/qgis-tips-tricks/tree/master/python/save_selected): Esse script seleciona dentro de cada camada do projeto feições usando uma expressão (feita na calculadora de campo) e salva a seleção em um arquivo, dentro da pasta da camada.

* [salva_em_gpkg](salva_em_gpkg/): Esse script salva todas as camadas vetoriais dentro de um projeto em um arquivo Geopackage.
